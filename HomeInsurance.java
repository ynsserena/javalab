public class HomeInsurance {
    private double amountInsured;
    private double excess;
    private double premium;

    public HomeInsurance(double amountInsured, double excess, double premium) {
        this.amountInsured = amountInsured;
        this.excess = excess;
        this.premium = premium;
    }
    
}