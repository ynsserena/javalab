public class TestAccount {
    public static void main(String[] args) {
        Account myAccount = new Account();
        myAccount.setName("Yining Song");
        myAccount.setBalance(19970901);
        System.out.println("Name: " + myAccount.getName());
        System.out.println("Balance: " + myAccount.getBalance());
        myAccount.addInterest();
        System.out.println("New Balance: " + myAccount.getBalance());

        Account[] arrayOfAccounts = new Account[5];
        double[] amounts = { 23, 5444, 2, 345, 34 };
        String[] names = { "Picard", "Ryker", "Worf", "Troy", "Data" };
        for (int i = 0; i < arrayOfAccounts.length; i++) {
            arrayOfAccounts[i] = new Account();
            arrayOfAccounts[i].setName(names[i]);
            arrayOfAccounts[i].setBalance(amounts[i]);
            System.out.println("Name: " + arrayOfAccounts[i].getName() +
             ", Balance: " + arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].addInterest();
            System.out.println("Name: " + arrayOfAccounts[i].getName() + 
                    ", New Balance: " + arrayOfAccounts[i].getBalance());
        }

    }
}